-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-02-2021 a las 13:46:15
-- Versión del servidor: 5.6.51
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `paxzupru_alfredo_prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`id`, `name`, `email`, `phone`, `address`, `password`, `active`, `date`) VALUES
(1, 'alfredo gonzalez', 'alfredogon82@yahoo.com', '3184240879', 'test no ', '1b01e2c0c85001ef5684bbf3a457f99e', '1', '2021-02-16 22:09:20'),
(2, 'prueba', 'test@yahoo.com', '3204240879', 'direccion prueba', '1b01e2c0c85001ef5684bbf3a457f99e', '1', '2021-02-16 23:28:14'),
(3, 'prueba nombre', 'test2@yahoo.com', '3204240879', 'direccion prueba 2', '1b01e2c0c85001ef5684bbf3a457f99e', '1', '2021-02-16 23:28:52'),
(4, 'king diamond', 'kingdiamond@kingdiamond.com', '3184240879', 'test address', '1b01e2c0c85001ef5684bbf3a457f99e', '1', '2021-02-17 20:13:07'),
(5, 'pruebausuario', 'prueba@prueba.com', '3184240879', 'dorecc', '1b01e2c0c85001ef5684bbf3a457f99e', '1', '2021-02-17 20:14:53'),
(6, 'ptest', 'test@test.com', '3184240879', 'direccion', '1b01e2c0c85001ef5684bbf3a457f99e', '1', '2021-02-17 20:34:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `status` varchar(45) DEFAULT 'initial',
  `date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `id_client`, `status`, `date`) VALUES
(1, 1, 'initial', '2021-02-17 00:23:39'),
(2, 1, 'initial', '2021-02-17 00:23:39'),
(3, 1, 'initial', '2021-02-18 00:39:10'),
(4, 1, 'initial', '2021-02-18 00:46:31'),
(5, 1, 'initial', '2021-02-18 00:47:36'),
(6, 1, 'initial', '2021-02-18 00:48:40'),
(7, 1, 'initial', '2021-02-18 00:50:06'),
(8, 1, 'initial', '2021-02-18 00:50:28'),
(9, 1, 'initial', '2021-02-18 00:50:42'),
(10, 1, 'initial', '2021-02-18 00:51:40'),
(11, 2, 'initial', '2021-02-18 00:57:01'),
(12, 2, 'initial', '2021-02-18 00:58:27'),
(13, 2, 'initial', '2021-02-18 00:58:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_code` varchar(45) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` varchar(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `product_code`, `name`, `price`, `image`, `active`) VALUES
(1, 'codssssigo', 'produasscto1s', 50000.00, NULL, '1'),
(2, 'codigo2', 'producto2', 6000.00, NULL, '1'),
(3, 'codigo3', 'producto3', 10000.00, NULL, '1'),
(4, 'codigoPrueba', 'PruebaProducto', 10000.00, NULL, '1'),
(6, '', '', 0.00, NULL, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_clients`
--

CREATE TABLE `products_clients` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products_clients`
--

INSERT INTO `products_clients` (`id`, `id_product`, `id_order`, `quantity`, `price`) VALUES
(1, 1, 1, 1, 5000.00),
(2, 3, 1, 1, 5000.00),
(3, 1, 6, 1, 50000.00),
(4, 2, 6, 4, 6000.00),
(5, 1, 10, 2, 100000.00),
(6, 2, 10, 3, 18000.00),
(7, 2, 11, 5, 30000.00),
(8, 1, 11, 10, 500000.00),
(9, 2, 12, 5, 30000.00),
(10, 2, 13, 5, 30000.00);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_UNIQUE` (`product_code`);

--
-- Indices de la tabla `products_clients`
--
ALTER TABLE `products_clients`
  ADD PRIMARY KEY (`id`,`id_product`),
  ADD KEY `fk_clients_has_products_products1_idx` (`id_product`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `products_clients`
--
ALTER TABLE `products_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
