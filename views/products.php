<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Products</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <a href="?id=add_product" type="button" class="btn btn-sm btn-outline-secondary">Agregar Producto</a>
          </div>
        </div>
      </div>

      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>#id</th>
              <th>Còdigo</th>
              <th>Nombre</th>
              <th>Precio</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              foreach ($_products as $product) { ?>
                <tr>
                  <td><?php echo $product['id'];?></td>
                  <td><?php echo $product['product_code'];?></td>
                  <td><?php echo $product['name'];?></td>
                  <td><?php echo $product['price'];?></td>
                  <td>
                    <?php if($product['active']=="1"){ ?>
                        <button target='products' status='active' id="<?php echo $product['id'];?>" type='button' class='activate btn btn-sm btn-outline-secondary'>Desactivar</button> 
                      <?php } else {  ?>
                        <button target='products' status='deactive' id="<?php echo $product['id'];?>" type='button' class='activate btn btn-sm btn-outline-secondary'>Activar</button> 
                      <?php } ?>
                      <a href="?id=update_product&id_product=<?php echo $product['id'];?>" type="button" class="btn btn-sm btn-outline-secondary">Editar</a>
                  </td>
                </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </main>
   