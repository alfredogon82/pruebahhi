<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Detalle Orden # <?php echo $_GET['id_detalle'];?></h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        </div>
      </div>

      <div class="table-responsive">
        <div>
            <div class="mb-3">
                <label for="" class="form-label">Nombre: <b><?php echo $_order['nombre'];?></b></label>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Email: <b><?php echo $_order['email'];?></b></label>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Telèfono: <b><?php echo $_order['phone'];?></b></label>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Estatus: <b><?php echo $_order['status'];?></b></label>
            </div>
            

            <div class="mb-3">
                <label for="" class="form-label">Detalle: <b></b></label>
                <br>
                <hr>
                <?php 
            foreach ($_order['info_orden'] as $orden) {
                echo "Nombre producto:<b>" .$orden["product_name"]."</b><br>";
                echo "Còdigo producto:<b>" .$orden["product_code"]."</b><br>";
                echo "Cantidad producto:<b>" .$orden["quantity"]."</b><br>";
                echo "Precio producto:<b>" .$orden["product_price"]."</b><br>";
                echo "<hr><br>";
            }
            ?>
            </div>

          


            <div class="mb-3">
                <label for="" class="form-label">Total: <b><?php echo $_order['total'];?></b></label>
            </div>
        </div>
      </div>
</main>

