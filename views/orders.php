<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Orders</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <a href="?id=add_order" type="button" class="btn btn-sm btn-outline-secondary">Agregar Orden</a>
          </div>
        </div>
      </div>

      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>#id</th>
              <th>Detalle</th>
              <th>Estado</th>
              <th>Total</th>
              <th>Fecha</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              foreach ($_orders as $order) { ?>
                <tr>
                  <td><?php echo $order['id'];?></td>
                  <td><a href="?id=order_detail&id_detalle=<?php echo $order['id'];?>">Ver Detalle</a></td>
                  <td><?php echo $order['status'];?></td>
                  <td><?php echo $order['total'];?></td>
                  <td><?php echo $order['date'];?></td>
                </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </main>
   