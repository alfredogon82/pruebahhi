<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Clients</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <a href="?id=add_client" type="button" class="btn btn-sm btn-outline-secondary">Agregar Cliente</a>
          </div>
        </div>
      </div>

      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>#id</th>
              <th>Nombre</th>
              <th>Email</th>
              <th>Telèfono</th>
              <th>Direcciòn</th>
              <th>Fecha</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              foreach ($_clients as $client) { ?>
                <tr>
                  <td><?php echo $client['id'];?></td>
                  <td><?php echo $client['name'];?></td>
                  <td><?php echo $client['email'];?></td>
                  <td><?php echo $client['phone'];?></td>
                  <td><?php echo $client['address'];?></td>
                  <td><?php echo $client['date'];?></td>
                  <td>
                    <?php if($client['active']=="1"){ ?>
                        <button target='clients' status='active' id="<?php echo $client['id'];?>" type='button' class='activate btn btn-sm btn-outline-secondary'>Desactivar</button> 
                      <?php } else {  ?>
                        <button target='clients' status='deactive' id="<?php echo $client['id'];?>" type='button' class='activate btn btn-sm btn-outline-secondary'>Activar</button> 
                      <?php } ?>
                    <a href="?id=update_client&id_client=<?php echo $client['id'];?>" type="button" class="btn btn-sm btn-outline-secondary">Editar</a>
                  </td>
                </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </main>
   