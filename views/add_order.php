<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Agregar orden </h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        </div>
      </div>

      <div class="table-responsive">
        <form action="service.php" method="POST">
            <div class="mb-3">
                <label for="" class="form-label">Cliente</label>
                <select name="id_cliente" class="form-select validate[required]" aria-label="Default select example">
                  <option selected>Seleccione el cliente</option>
                  <?php 
                  foreach ($_clients as $client) { ?>
                    <option value="<?php echo $client['id'];?>"><?php echo $client['name'];?></option>
                  <?php } ?>
                </select>
                <div id="" class="form-text">Seleccione el cliente al cual se le añadira la orden</div>
            </div>
            <div class="mb-2 toClone">
                <label for="" class="form-label"></label>
                <select name="producto[]" class="form-select validate[required]" aria-label="Default select example">
                  <option selected>Seleccione el producto</option>
                  <?php 
                  foreach ($_products as $product) { ?>
                    <option value="<?php echo $product['id'];?>"><?php echo $product['name'];?></option>
                  <?php } ?>
                </select>
                <select name="cantidad[]" class="form-select validate[required]" aria-label="Default select example">
                  <option selected>Seleccione la cantidad de producto</option>
                  <?php 
                  for ($i=1;$i<=10;$i++) { ?> 
                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                  <?php }  ?>
                </select>
            </div>
            <div class="toAppend">
            </div>
            <div class="mb-2">
                <label id="agregar" class="form-label">Añadir producto</label> ||
                <label id="eliminar" class="form-label">Eliminar producto</label>
            </div>
            <input type="hidden" name="task" value="add_order">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </main>
    <script>
      $(document).ready(function(){

        $("#agregar").click(function() {
          $(".toClone").clone().appendTo(".toAppend");
        });

        $(document).on('click','#eliminar',function() {
          alert("elimina");
          $(this).closest(".toClone").remove();
        });


      });
    </script>

    

