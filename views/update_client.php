<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Cliente <?php echo $client['name']?> </h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        </div>
      </div>

      <div class="table-responsive">
        <?php //var_dump($client);?>
        <form action="service.php" method="POST">
            <div class="mb-3">
                <label for="" class="form-label">Nombre</label>
                <input  min="8" max="20" value="<?php echo $client['name']?>" type="name" class="form-control validate[required,custom[onlyLetterSp]]" name="name" id="name" aria-describedby="">
                <div id="" class="form-text"></div>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Email</label>
                <input  min="6" max="20" value="<?php echo $client['email']?>" type="email" class="form-control validate[required,custom[email]]" name="email" id="email" aria-describedby="">
                <div id="" class="form-text"></div>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Telèfono</label>
                <input  min="6" max="10" value="<?php echo $client['phone']?>" type="input" class="form-control validate[required,custom[number],minSize[10],maxSize[10]]" name="phone" id="phone" aria-describedby="">
                <div id="" class="form-text"></div>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Direcciòn</label>
                <input value="<?php echo $client['address']?>" type="input" class="form-control  validate[required, minSize[5],maxSize[30]]" name="address" id="address" aria-describedby="">
                <div id="" class="form-text"></div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input min="6" max="10" type="password" class="form-control validate[minSize[5],maxSize[10]]" placeholder="***************" name="password" id="password">
                <div id="" class="form-text">Si no lo quiere actualizar, deje el campo vacìo</div>
            </div>
            <input type="hidden" name="task" value="update_user">
            <input type="hidden" name="id" value="<?php echo $client['id'];?>">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </main>

