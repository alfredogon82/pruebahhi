<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Reportes</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
          </div>
        </div>
      </div>

      <div class="table-responsive">
            <div class="mb-3">
                <label for="" class="form-label">Cliente</label>
                <select id="cliente" name="id_cliente" class="form-select validate[required]" aria-label="Default select example">
                  <option selected>Seleccione el cliente</option>
                  <?php 
                  foreach ($_clients as $client) { ?>
                    <option value="<?php echo $client['id'];?>"><?php echo $client['name'];?></option>
                  <?php } ?>
                </select>
                <div id="" class="form-text">Seleccione el cliente del cual desea obtener un reporte</div>
            </div>
      </div>
      <div class="table-responsive">
      <div class="mb-3">
                <label for="" class="form-label">Producto</label>
                <select name="producto" id="producto" class="form-select validate[required]" aria-label="Default select example">
                  <option selected>Seleccione el producto</option>
                  <?php 
                  foreach ($_products as $product) { ?>
                    <option value="<?php echo $product['id'];?>"><?php echo $product['name'];?></option>
                  <?php } ?>
                </select>
                <div id="" class="form-text">Seleccione el producto del cual desea obtener un reporte</div>
            </div>
       </div>
       <div id="contenido">
       </div>
    </main>
    <script>
      $(document).ready(function(){

        $("#producto").change(function() {
        var producto_selected = $("#producto option:selected").val();
        if(producto_selected.length>0){
        $.ajax({
            type: "POST",
              url: "service.php",
              data: {
              id_product: producto_selected, 
              task:"getReportByProductId"
            }
        }).done(function(data){
          var obj = $.parseJSON(data);
          console.log(obj);
          $("#contenido").html("");
          var content = "";
          for (var i = 0; i< obj.length; i++) {
            content += "<label>Nombre:</label> "+obj[i].nombre+"<br>";
            content += "<label>Email:</label> "+obj[i].email+"<br>";
            content += "<label>Fecha:</label> "+obj[i].fecha+"<br>";
            content += "<label>Telefono:</label> "+obj[i].phone+"<br>";
            content += "<label>Status:</label> "+obj[i].status+"<br>";
            content += "<label>Total:</label> "+obj[i].total+"<br>";
            content += "<label>Detalle del pedido:</label><br>";

            console.log(obj[i]);
            console.log("aqui "+obj[i]['info_orden'].length);
            for (var k = 0; k<obj[i]['info_orden'].length; k++) {
              content += "<hr>";
              content += "<label>Orden:</label> "+obj[i]['info_orden'][k]['order']+"<br>";
              content += "<label>Producto:</label> "+obj[i]['info_orden'][k]['product_name']+"<br>";
              content += "<label>Codigo Producto:</label> "+obj[i]['info_orden'][k]['product_code']+"<br>";
              content += "<label>Precio Producto:</label> "+obj[i]['info_orden'][k]['product_price']+"<br>";
              content += "<label>Cantidad Producto:</label> "+obj[i]['info_orden'][k]['quantity']+"<br>";
              content += "<hr>";
              console.log(obj[i]['info_orden'][k]);
              console.log(obj[i]['info_orden'][k]['product_name']);
            }
          }
          $("#contenido").html(content);
        
        
        });  
        }
      });

      $("#cliente").change(function() {
        var cliente_selected = $("#cliente option:selected").val();
        if(cliente_selected.length>0){
        $.ajax({
            type: "POST",
              url: "service.php",
              data: {
              id_cliente: cliente_selected, 
              task:"getReportByClienteId"
            }
        }).done(function(data){
          var obj = $.parseJSON(data);
          console.log(obj);
          $("#contenido").html("");
          var content = "";
          for (var i = 0; i< obj.length; i++) {
            content += "<label>Nombre:</label> "+obj[i].nombre+"<br>";
            content += "<label>Email:</label> "+obj[i].email+"<br>";
            content += "<label>Fecha:</label> "+obj[i].fecha+"<br>";
            content += "<label>Telefono:</label> "+obj[i].phone+"<br>";
            content += "<label>Status:</label> "+obj[i].status+"<br>";
            content += "<label>Total:</label> "+obj[i].total+"<br>";
            content += "<label>Detalle del pedido:</label><br>";

            console.log(obj[i]);
            console.log("aqui "+obj[i]['info_orden'].length);
            for (var k = 0; k<obj[i]['info_orden'].length; k++) {
              content += "<hr>";
              content += "<label>Orden:</label> "+obj[i]['info_orden'][k]['order']+"<br>";
              content += "<label>Producto:</label> "+obj[i]['info_orden'][k]['product_name']+"<br>";
              content += "<label>Codigo Producto:</label> "+obj[i]['info_orden'][k]['product_code']+"<br>";
              content += "<label>Precio Producto:</label> "+obj[i]['info_orden'][k]['product_price']+"<br>";
              content += "<label>Cantidad Producto:</label> "+obj[i]['info_orden'][k]['quantity']+"<br>";
              content += "<hr>";
              console.log(obj[i]['info_orden'][k]);
              console.log(obj[i]['info_orden'][k]['product_name']);
            }
          }
          $("#contenido").html(content);
        });  
        }
      });
      });
    </script>
   