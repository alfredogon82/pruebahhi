<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Agregar Producto</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        </div>
      </div>

      <div class="table-responsive">
        <form action="service.php" method="POST">
            <div class="mb-3">
                <label for="" class="form-label">Còdigo del producto</label>
                <input  min="8" max="20" value="" type="name" class="form-control  validate[required, minSize[5],maxSize[30]]" name="product_code" id="product_code" aria-describedby="">
                <div id="" class="form-text"></div>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Nombre</label>
                <input  min="6" max="20" value="" type="input" class="form-control validate[required, minSize[5],maxSize[30]]" name="name" id="name" aria-describedby="">
                <div id="" class="form-text"></div>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Precio</label>
                <input  min="3 max="20" value="" type="number" class="form-control validate[required,custom[number],minSize[4],maxSize[10]]" name="price" id="price" aria-describedby="">
                <div id="" class="form-text"></div>
            </div>
            <input type="hidden" name="task" value="add_product">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </main>

