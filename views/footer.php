</div>
</div>


    <script src="assets/dist/js/bootstrap.bundle.min.js"></script>

      <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
    </script>
    <script src="js/dashboard.js">
    </script>
    </body>
</html>

<script>
    $(document).ready(function(){

      $("form").validationEngine('attach', {
          onValidationComplete: function(form, status) {
              if (status == true) {
                  $('.primary').css({
                      "display": "none"
                  });
                  return true;
              }
          }
      })




      $(".activate").click(function(){

        if($(this).attr("status")=="active"){
          var action = "deactivate";
        } else {
          var action = "activate";
        }

        var target = $(this).attr("target");
        var id = $(this).attr("id");

        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
              id: id, 
              action: action,
              target: target,
              task:"activate"
            }
        }).done(function(data){
            check = $.trim(data);
            if(check=="1"){
              window.location.reload();
            }
        });  
    });

    });
    </script>

