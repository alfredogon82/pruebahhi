    <?php 
    error_reporting(-1);
    require_once("classes/database.class.php");
    require_once("config/config.php");

    

    class Contents {

        public function getClients(){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM clients where active='1'";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $result = $query->fetchAll();

                return $result;

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function getProducts(){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM products where active='1'";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $result = $query->fetchAll();

                return $result;

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function getProductById($id_product){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM products where id='".$id_product."'";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $result = $query->fetch();

                return $result;

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function getClientById($id_client){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM clients where id='".$id_client."'";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $result = $query->fetch();

                return $result;

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function getOrders(){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM orders";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $result = $query->fetchAll();

                return $result;

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function getOrderById($id_order){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM orders where id='".$id_order."'";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $result = $query->fetch();

                return $result;

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function getOrderDetailById($id_order){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM products_clients where id_order='".$id_order."' ";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $result = $query->fetchAll();

                $_order_detail = array();
                $_total = array();

                $i=0;
                foreach ($result as $rslt) {
                    $product_info = $this->getProductById($rslt['id_product']);
                    
                    $_order_detail[$i]['order'] = $id_order;
                    $_order_detail[$i]['product_name'] = $product_info['name'];
                    $_order_detail[$i]['product_code'] = $product_info['product_code'];
                    $_order_detail[$i]['product_price'] = $rslt['price'];
                    $_order_detail[$i]['quantity'] = $rslt['quantity'];
                    array_push($_total,$rslt['price']);
                    $i++;
                }

                $order_info = $this->getOrderById($id_order);

                $client_info = $this->getClientById($order_info['id_client']);

                $total = array_sum($_total);
                $_rslt = array("info_orden" => $_order_detail, "total" => $total, "fecha" => $order_info['date'], "status" => $order_info['status'], "nombre" => $client_info['name'], "email" => $client_info['email'], "phone" => $client_info['phone']);

                //die(var_dump($_rslt));

                return $_rslt; 

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function activate($id, $action, $target){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                if($action=="activate"){
                    $active = "1";
                } else {
                    $active = "0";
                }

                $sql = "UPDATE ".$target." SET active='".$active."' WHERE id=".$id;
                $q = $conn->prepare($sql);
                $check = $q->execute();
                if($check=="true"){
                    echo 1;
                } else {
                    echo 0;
                }

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function add_user($name, $email, $phone, $address, $password){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "INSERT INTO clients (name, email, phone, address, password, active) VALUES (?,?,?,?,?,?)";
                $stmt = $conn->prepare($sql);
                $pass = md5($password);
                $active = 1;
                $check = $stmt->execute([$name, $email, $phone, $address, $pass, $active]);

                if($check=="true"){
                    $succesful = _DOMAIN."?message=1";
                    echo "<script>window.location.href='".$succesful."'</script>";
                } else {
                    $unsuccesful = _DOMAIN."?message=0";
                    echo "<script>window.location.href=0'".$unsuccesful."'</script>";
                }

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function insert_order($id_client){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "INSERT INTO orders (id_client, status) VALUES (?,?)";
                $stmt = $conn->prepare($sql);
                $status='initial';
                $check = $stmt->execute([$id_client, $status]);
                $last_id = $conn->lastInsertId();
                
                return $last_id;                

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function insert_product_client($id_product, $id_order, $quantity){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $product_info = $this->getProductById($id_product);

                $price = $quantity*$product_info['price'];

                $sql = "INSERT INTO products_clients (id_product, id_order, quantity, price) VALUES (?,?,?,?)";
                $stmt = $conn->prepare($sql);
                $check = $stmt->execute([$id_product, $id_order, $quantity, $price]);
                

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function update_user($id, $name, $email, $phone, $address, $password){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                if(empty($password)){
                    $sql = "UPDATE clients SET name='".$name."', email='".$email."', phone='".$phone."', address='".$address."' WHERE id=".$id;
                } else {
                    $pass = md5($password);
                    $sql = "UPDATE clients SET name='".$name."', email='".$email."', phone='".$phone."', address='".$address."', password='".$pass."' WHERE id=".$id;
                }

                $q = $conn->prepare($sql);
                $check = $q->execute();
                if($check=="true"){
                    $succesful = _DOMAIN."?message=2";
                    echo "<script>window.location.href='".$succesful."'</script>";
                } else {
                    $unsuccesful = _DOMAIN."?message=3";
                    echo "<script>window.location.href='".$unsuccesful."'</script>";
                }

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }


        public function add_product($name, $product_code, $price){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "INSERT INTO products (product_code, name,  price, active) VALUES (?,?,?,?)";
                $stmt = $conn->prepare($sql);
                $active = 1;
                $check = $stmt->execute([$product_code, $name, $price, $active]);


                if($check=="true"){
                    $succesful = _DOMAIN."?message=1";
                    echo "<script>window.location.href='".$succesful."'</script>";
                } else {
                    $unsuccesful = _DOMAIN."?message=0";
                    echo "<script>window.location.href='".$unsuccesful."'</script>";
                }

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function update_product($id, $name, $product_code, $price){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "UPDATE products SET name='".$name."', product_code='".$product_code."', price='".$price."' WHERE id=".$id;
                //die(var_dump($sql));
                $q = $conn->prepare($sql);
                $check = $q->execute();
                if($check=="true"){
                    $succesful = _DOMAIN."?message=2";
                    echo "<script>window.location.href='".$succesful."'</script>";
                } else {
                    $unsuccesful = _DOMAIN."?message=3";
                    echo "<script>window.location.href='".$unsuccesful."'</script>";
                }

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function getReportByClienteId($id_cliente){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM orders where id_client='".$id_cliente."'";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $results = $query->fetchAll();

                $_total = array();

                foreach ($results as $result){
                    $check = $this->getOrderDetailById($result['id']);
                    array_push($_total, $check);
                }

                echo json_encode($_total);

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }

        public function getReportByProductId($id_product){ 

            try{ 
                
                $db   = new database();
                $conn = $db->conn();

                $sql = "SELECT * FROM products_clients where id_product='".$id_product."'";
                $query = $conn->prepare($sql);
                $executed = $query->execute(); 
                $results = $query->fetchAll();

                $_total = array();

                foreach ($results as $result){
                    //$check = $this->getProductById($result['id_product']);
                    $check = $this->getOrderDetailById($result['id_order']);
                    
                    //$_total = array("check" => $check, "order" => $checkOrder); 
                    
                    array_push($_total, $check);
                    
                }

                echo json_encode($_total);

            } catch( PDOException $e ) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return false;
            }
        }


    }