<?php


class Database{

    public function conn() {

        require("config/config.php");

        $dbhost = _DBHOST;
        $dbname = _DBNAME;        
        $dbuser = _DBUSER;
        $dbpass = _DBPASSWORD;

        if($conn = new PDO("mysql:host=".$dbhost.";dbname=".$dbname, $dbuser, $dbpass,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'))){
            return($conn);
         }
         else {
            return null;
        }
    }
}

