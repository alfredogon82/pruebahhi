<?php
require_once("config/config.php");
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
$_input_post = filter_input_array(INPUT_POST);
if (isset($_input_post["task"])) {
    $task = $_input_post["task"];
    switch ($task) {
    	case 'activate':

	        require("classes/content.class.php");
			$content = new Contents();
			$id = $_POST['id'];
			$action = $_POST['action'];
			$target = $_POST['target'];
	        $content->activate($id, $action, $target);
		
		break;
		
		case 'add_user':

	        require("classes/content.class.php");
	        $content = new Contents();
			$name = $_input_post['name'];
			$email = $_input_post['email'];
			$phone = $_input_post['phone'];
			$address = $_input_post['address'];
			$password = $_input_post['password'];
			$content->add_user($name, $email, $phone, $address, $password);
		
		break;

        case 'update_user':

	        require("classes/content.class.php");
	        $content = new Contents();
			$id = $_input_post['id'];
			$name = $_input_post['name'];
			$email = $_input_post['email'];
			$phone = $_input_post['phone'];
			$address = $_input_post['address'];
			$password = $_input_post['password'];
			$content->update_user($id, $name, $email, $phone, $address, $password);
		
		break;


		case 'add_product':

	        require("classes/content.class.php");
	        $content = new Contents();
			$name = $_input_post['name'];
			$product_code = $_input_post['product_code'];
			$price = $_input_post['price'];
			$content->add_product($name, $product_code, $price);
		
		break;
		
		case 'add_order':

	        require("classes/content.class.php");
	        $content = new Contents();
			$id_client = $_POST['id_cliente'];
			$id_order = $content->insert_order($id_client);

			$nmr_products = count($_POST['producto']);

			$producto = $_POST['producto'];
			$cantidad = $_POST['cantidad'];

			for ($i=0;$i<=$nmr_products;$i++) { 
				$content->insert_product_client($producto[$i], $id_order, $cantidad[$i]);
			}
            $redirect = _DOMAIN."?id=orders";
			echo "<script>window.location.href='".$redirect."'</script>";

			
        break;
		
		case 'update_product':

	        require("classes/content.class.php");
	        $content = new Contents();
			$id = $_input_post['id'];
			$name = $_input_post['name'];
			$product_code = $_input_post['product_code'];
			$price = $_input_post['price'];
			$content->update_product($id, $name, $product_code, $price);
		
		break;

				
		case 'getReportByClienteId':

	        require("classes/content.class.php");
	        $content = new Contents();
			$id_cliente = $_input_post['id_cliente'];
			$content->getReportByClienteId($id_cliente);
		
		break;

		case 'getReportByProductId':

	        require("classes/content.class.php");
	        $content = new Contents();
			$id_product = $_input_post['id_product'];
			$content->getReportByProductId($id_product);
		
		break;

		
		
		

        default:
        echo "No task!";
        break;
	}
}
	