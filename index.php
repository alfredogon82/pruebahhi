    <?php
    if (!isset($_GET["id"])) {
      $_GET["id"] = 'main';
    }

    //var_dump($_GET['id']);

    switch ($_GET["id"]) {
        case 'main':
          require("classes/content.class.php");
          $content = new Contents();
          $_clients = $content->getClients();
          //var_dump($_clients);
          include("views/header.php");
          include("views/clients.php");
          include("views/footer.php");                 
        break;
        case 'update_client':
          require("classes/content.class.php");
          $content = new Contents();
          $id_client = $_GET['id_client'];
          $client = $content->getClientById($id_client);
          include("views/header.php");
          include("views/update_client.php");
          include("views/footer.php");                 
        break;
        case 'update_product':
          require("classes/content.class.php");
          $content = new Contents();
          $id_product = $_GET['id_product'];
          $product = $content->getProductById($id_product);
          include("views/header.php");
          include("views/update_product.php");
          include("views/footer.php");                 
        break;
        case 'add_client':
          include("views/header.php");
          include("views/add_client.php");
          include("views/footer.php");                 
        break;
        
        case 'add_product':
          include("views/header.php");
          include("views/add_product.php");
          include("views/footer.php");                 
        break;

        case 'products':
          require("classes/content.class.php");
          $content = new Contents();
          $_products = $content->getProducts();
          include("views/header.php");
          include("views/products.php");  
          include("views/footer.php");                      
          break;

        case 'orders':
          require("classes/content.class.php");
          $content = new Contents();
          $_orders = $content->getOrders();
          include("views/header.php");
          include("views/orders.php");  
          include("views/footer.php");                       
        break;

        case 'order_detail':
          require("classes/content.class.php");
          $content = new Contents();
          $id_detalle = $_GET['id_detalle'];
          $_order = $content->getOrderDetailById($id_detalle); 
          include("views/header.php");
          include("views/order_detail.php");  
          include("views/footer.php");                       
        break;

        case 'add_order':
          require("classes/content.class.php");
          $content = new Contents();
          $_clients = $content->getClients();
          $_products = $content->getProducts();
          include("views/header.php");
          include("views/add_order.php");
          include("views/footer.php");                 
        break;

        case 'reports':
          require("classes/content.class.php");
          $content = new Contents();
          $_clients = $content->getClients();
          $_products = $content->getProducts();
          include("views/header.php");
          include("views/reports.php");
          include("views/footer.php");                 
        break;
    }

?>

